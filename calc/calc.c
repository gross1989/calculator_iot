/**
* calc.h
* 
* calc module implementation
*/

#include "calc.h"

int calc_is_supported( Op_type operation)
{
	switch(operation)
	{
		case OP_ADD:
			#if (defined(CANADD) || defined(CANALL))
			return 1;
			#else
			return 0;
			#endif
		
		case OP_SUB:
			#if (defined(CANSUB) || defined(CANALL))
			return 1;
			#else
			return 0;
			#endif
		
		case OP_MUL:
			#if (defined(CANMUL) || defined(CANALL))
			return 1;
			#else
			return 0;
			#endif
			
		case OP_DIV:
			#if (defined(CANDIV) || defined(CANALL))
			return 1;
			#else
			return 0;
			#endif
		
		default: 
			return 0;
	}
}

int calc_do_operation( Op_type operation, int operand1, int operand2)
{
	(void) operand1;
	(void) operand2;
	
	switch(operation)
	{
		case OP_ADD:
			#if (defined(CANADD) || defined(CANALL))
			return operand1 + operand2;
			#else
			return -1;
			#endif
		
		case OP_SUB:
			#if (defined(CANSUB) || defined(CANALL))
			return operand1 - operand2;
			#else
			return -1;
			#endif
		
		case OP_MUL:
			#if (defined(CANMUL) || defined(CANALL))
			return operand1 * operand2;
			#else
			return -1;
			#endif
			
		case OP_DIV:
			#if (defined(CANDIV) || defined(CANALL))
			return operand1 / operand2;
			#else
			return -1;
			#endif
		
		default:
			return -1;
	}
}
