/**
* calc.h
* 
* calc module declarations
*/

#ifndef CALC_H
#define CALC_H

/* codes for the supported operations */
typedef enum 
{
	OP_ADD = 0, //'+',
	OP_SUB = 1, //'-',
	OP_MUL = 2, //'*',
	OP_DIV = 3, //'/'
	OP_DUMMY = 0x7fffffff // so that Op_type is 4 bytes big
} Op_type;


/* returns true if operation is supported */
int calc_is_supported( Op_type operation);

/* returns result if operation is supported or -1 otherwise */
int calc_do_operation( Op_type operation, int operand1, int operand2);

#endif
