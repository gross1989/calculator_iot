#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>


#include "shell.h"
#include "../io.h"


int fetchOperand(int *operand);
int fetchOperator(Op_type *op);
int isOperator(char *op);
int isDigit(char *c);
int atoOp(char *input, Op_type *op);


/*------------------------------------------------------------------------------------*/


int io_init( void) {
	return 0;
}

/*
is called from core.c
enables user to input operands and operation_type from console
includes error checking
*/
void io_read( Op_type *operation, int *operand1, int *operand2)
{
	int status = 0;

	while(status<4){

		//simple state machine approach
		switch(status){
			//(0,0,0)
			case 0:
				puts("Type in Operand 1:");
				if(fetchOperand(operand1)==0){
					status = 1;
				}else{
					puts("Maybe try again.");
					break;
				}
			//(1,0,0)
			case 1:
				puts("Type in the operation:");
				if(fetchOperator(operation)==0){
					status = 2;
				}else{
					puts("Maybe try again.");
					break;
				}
			//(1,1,0)
			case 2:
				puts("Type in Operand 2:");
				if(fetchOperand(operand2)==0){
					status = 3;
				}else{
					puts("Maybe try again.");
					break;
				}
			//(1,1,1)
			case 3:
				return;
		}
	}


	return;
}

void io_show( int result)
{
	printf("Result: %d\n", result);
}

int fetchOperand(int *operand){
	int i;
  	char buffer[SHELL_DEFAULT_BUFSIZE];
	if ( fgets ( buffer, SHELL_DEFAULT_BUFSIZE, stdin ) != NULL ) {
		// fgets adds '\0'
		// buffer[strlen ( buffer ) - 1] = '\0';
		if ( isDigit ( buffer ) == 0 ) {
			i = atoi ( buffer );

		}else{
			return 1;
		}
	}else{
		return 1;
	}
	*operand = i;
	return 0;

}

// checks if a given char sequence contain digits only
int isDigit(char *c){
	unsigned int i;
	// last byte is '\n'
	for(i = 0; i<strlen(c)-1; i++){
		if(!isdigit((unsigned char)c[i])){
			return 1;
		}
	}
	return 0;
}


int fetchOperator(Op_type *op){
	Op_type operator;
  	char buffer[SHELL_DEFAULT_BUFSIZE];
	if ( fgets ( buffer, SHELL_DEFAULT_BUFSIZE, stdin ) != NULL ) {
		// fgets add '\0'
		// buffer[strlen ( buffer ) - 1] = '\0';
		if ( isOperator( buffer ) == 0 ) {
			atoOp ( buffer, &operator);
			*op = operator;
	    		return 0;
		}else{
			return 1;
		}
	}else{
		return 1;
	}

	return 0;
}

int isOperator(char *input){
	// last byte is '\n'
	if(strlen(input)>2){
		return 1;
	}
	if(input[0]=='+'||input[0]=='-'||input[0]=='*'||input[0]==':'){
		return 0;
	}else{
		return 1;
	}
}

/*
analogy to atoi
maps char input to operation_type defined in calc.h
*/
int atoOp(char *input, Op_type *op){
	switch(input[0]){
		case '+':
			*op = OP_ADD;
			break;
		case '-':
			*op = OP_SUB;
			break;
		case '*':
			*op = OP_MUL;
			break;
		case ':':
			*op = OP_DIV;
			break;
		default:
			return 1;
	}
	return 0;

}

