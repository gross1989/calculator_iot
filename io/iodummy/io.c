#include <stdio.h>
#include "xtimer.h"

#include "../io.h"


int io_init( void) {
	return 0;
}

void io_read( Op_type *operation, int *operand1, int *operand2)
{
	*operation = OP_ADD;
	*operand1 = 10;
	*operand2 = 20;

	printf("dummy");

	xtimer_sleep(1);
}

void io_show( int result)
{
	printf("%d\n", result);
}
