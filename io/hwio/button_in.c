/**
 * button_in.c
 * 
 * Hardware input system implementation
 */

#include "button_in.h"

// testing
#include <stdio.h>

// RIOT
#include "mutex.h"
#include "xtimer.h"
#include "periph/gpio.h"

// custom
#include "hwio.h"

// defines for polling
#define POLL_WAIT 5000U     // wait between ticks in us
#define TICKS_THRESHOLD 6   // must press for that many ticks to trigger

// variables for polling
bool lastPinStates[NUM_BUTTONS];
unsigned int pinTimeCounters[NUM_BUTTONS];

// PRIVATE functions

// read a value with the input system
void read( int* value )
{
	printf( "hwio: Starting to read\n" );
	
	*value = 0;
	
	// Init polling variables
	for( unsigned int button = 0; button < NUM_BUTTONS; button++ )
	{
		lastPinStates[button] = false;
		pinTimeCounters[button] = 0;
	}
	
	// Start polling
	while( true )
	{
		// Check each button
		for( unsigned int button = 0; button < NUM_BUTTONS; button++ )
		{
			bool state = gpio_read( get_input_pin( button ) ) != 0;
			
			if( state == lastPinStates[button] )
			{
				// State unchanged, continue counting
				pinTimeCounters[button]++;
			}
			else
			{
				// State changed!
				if( lastPinStates[button] )
				{
					// Button was released
					printf( "hwio: Got button %d %s for %d ticks\n",
							button,
							lastPinStates[button] ? "high" : "low",
							pinTimeCounters[button] );
					
					if( pinTimeCounters[button] >= TICKS_THRESHOLD )
					{
						// Button was pressed for at least TICKS_THRESHOLD ticks
						if( button == BUTTON_ENTER )
						{
							io_show( 0 );
							printf( "hwio: Done reading!\n" );
							
							return;
						}
						else
						{
							int button_value = ( button - 1 ) % 2;
							*value <<= 1;
							printf( "hwio: adding %d:", button_value );
							*value += button_value;
							printf( " value -> %d\n", *value );
							
							io_show( *value );
						}
					}
				}
				
				// Set state and reset counter
				lastPinStates[button] = state;
				pinTimeCounters[button] = 0;
			}
		}
		
		xtimer_usleep( POLL_WAIT );
	}
}

// HWIO-PUBLIC

// configure button pins, called by io_init()
bool init_buttons( void )
{
	gpio_idx_t expected_in_pins = NUM_BUTTONS;
	
	if( num_input_pins < expected_in_pins )
	{
		printf( "hwio: Error: not enough input pins initialized" );
		return false;
	}
	
	printf( "hwio: Initializing %d input pins.\n", num_input_pins );
	
	for( gpio_idx_t button = 0; button < num_input_pins; button++ )
	{
		if( gpio_init( get_input_pin( button ), GPIO_IN_PD ) != 0 )
		{
			printf( "hwio: Failed initializing pin %x\n", (unsigned int) get_input_pin( button ) );
			return false;
		}
	}
	
	return true;
}

// PUBLIC

// read a calculation
void io_read( Op_type* operation, int* operand1, int* operand2 )
{
	read( operand1 );
	printf( "hwio: Read operand %d\n", *operand1 );
	int oper;
	read( &oper );
	*operation = oper;
	printf( "hwio: Read operation %d\n", (int) *operation );
	read( operand2 );
	printf( "hwio: Read operand %d\n", *operand2 );
}
