/**
 * out.h
 * 
 * Definitions for LED and segment display output
 */

#ifndef _OUT_H
#define _OUT_H

// check flags
#if defined( OUT_LED ) == defined ( OUT_SEG ) // both defined or both not defined
#error "Exactly one of the flags OUT_LED and OUT_SEG must be defined for hardware I/O"
#endif

// stdlib
#include <stdbool.h>
#include <stdint.h>

// RIOT
#include "board.h"

// custom
#include "hwio.h"

// public interface
#include "../io.h"

// LED output
#ifdef OUT_LED
// default number of leds
#ifndef NUM_LEDS
#define NUM_LEDS (4)
#endif

#define NUM_OUTPUT_PINS (4)
#endif

// seven-segment display output
#ifdef OUT_SEG
// default number of displays
#ifndef NUM_DISPLAYS
#define NUM_DISPLAYS (2)
#endif

#define NUM_OUTPUT_PINS ( 7 * NUM_DISPLAYS )
#endif


typedef uint8_t output_t;

#endif /*_OUT_H*/
