/**
 * button_in.h
 * 
 * Definitions for hardware input system
 */

#ifndef _BUTTON_IN_H
#define _BUTTON_IN_H

// stdlib
#include <stdbool.h>

// public interface
#include "../io.h"

// number of buttons
#define BUTTON_ENTER (0)
#define BUTTON_0 (1)
#define BUTTON_1 (2)
#define NUM_BUTTONS (3)

#define NUM_INPUT_BITS (4)

// configure button pins, called by io_init()
bool init_buttons( void );

#endif /*_BUTTON_IN_H*/
