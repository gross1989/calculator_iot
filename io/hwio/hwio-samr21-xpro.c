/**
 * hwio-samr21-xpro.c
 * 
 * Platform-dependent definitions for the samr21-xpro board
 */

#include "hwio.h"

#include "periph/gpio.h"

#define DEF_NUM_GPIO_PINS (17)

const gpio_idx_t NUM_GPIO_PINS = DEF_NUM_GPIO_PINS;

const gpio_t GPIO_PINS[DEF_NUM_GPIO_PINS] = {
// Extension Header EXT1
	GPIO_PIN( PA,  6 ),
	GPIO_PIN( PA,  7 ),
	GPIO_PIN( PA, 13 ),
	GPIO_PIN( PA, 28 ), // also SW0
	GPIO_PIN( PA, 18 ),
	GPIO_PIN( PA, 19 ), // also LED0
	GPIO_PIN( PA, 22 ),
	GPIO_PIN( PA, 23 ),
	GPIO_PIN( PA, 16 ),
	GPIO_PIN( PA, 17 ),
//	GPIO_PIN( PA,  5 ), // UART
//	GPIO_PIN( PA,  4 ), // UART
	GPIO_PIN( PB,  3 ),
	GPIO_PIN( PB, 22 ),
	GPIO_PIN( PB,  2 ),
	GPIO_PIN( PB, 23 ),
// Extension Header EXT3
	GPIO_PIN( PA, 15 ),
	GPIO_PIN( PA,  8 ),
	GPIO_PIN( PA, 14 )
};
