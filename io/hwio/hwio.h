/**
 * hwio.h
 * 
 * General hardware IO function definitions
 */

#ifndef _HWIO_H
#define _HWIO_H

#include <stdbool.h>
#include <stdint.h>

#include "periph/gpio.h"

// public interface (hwio.c)
#include "../io.h"

// platform-dependent (hwio-xxx.c)

typedef uint8_t gpio_idx_t;

extern const gpio_idx_t NUM_GPIO_PINS;
extern const gpio_t GPIO_PINS[];

// platform-independent (hwio.c)

extern gpio_idx_t num_input_pins;
extern gpio_idx_t num_output_pins;

gpio_t get_input_pin( gpio_idx_t idx );
gpio_t get_output_pin( gpio_idx_t idx );

#endif /*_HWIO_H*/
