/**
 * hwio.c
 * 
 * General hardware IO function implementations
 */

#include "hwio.h"

#include <stdio.h>

#include "periph/gpio.h"

#include "button_in.h"

#include "out.h"

gpio_idx_t num_input_pins;
gpio_idx_t num_output_pins;

// private function definitions
bool init_gpio( gpio_idx_t num_in, gpio_idx_t num_out );
bool init_output( void );

// PUBLIC

int io_init( void )
{
	if( !init_gpio( NUM_BUTTONS, NUM_OUTPUT_PINS ) )
		return -1;
	
	if( !init_output() )
		return -1;
	
	if( !init_buttons() )
		return -1;
	
	return 0;
}

// PRIVATE

// init pin indices
bool init_gpio( gpio_idx_t num_in, gpio_idx_t num_out )
{
	if( num_input_pins + num_output_pins > NUM_GPIO_PINS )
	{
		printf( "hwio: Trying to initialize %d pins, but only %d are available.\n", num_input_pins + num_output_pins, NUM_GPIO_PINS );
		return false;
	}
	
	num_input_pins = num_in;
	num_output_pins = num_out;
	
	return true;
}

// configure output pins
bool init_output( void )
{
	gpio_idx_t expected_out_pins = NUM_OUTPUT_PINS;
	
	if( num_output_pins < expected_out_pins )
	{
		printf( "hwio: Error: not enough output pins initialized" );
		return false;
	}
	
	printf( "hwio: Initializing %d output pins.\n", num_output_pins );
	
	for( gpio_idx_t i = 0; i < num_output_pins; i++ )
	{
		if( gpio_init( get_output_pin( i ), GPIO_OUT ) != 0 )
		{
			printf( "hwio: Initializing output pin %d at %x failed!\n", i, (unsigned int) GPIO_PINS[num_input_pins + i] );
			return false;
		}
		
		gpio_clear( get_output_pin( i ) );
		
		printf( "hwio: Initialized output pin %d at %x\n", i, (unsigned int) GPIO_PINS[num_input_pins + i] );
	}
	
	return true;
}

gpio_t get_input_pin( gpio_idx_t idx )
{
	return GPIO_PINS[idx];
}

gpio_t get_output_pin( gpio_idx_t idx )
{
	return GPIO_PINS[num_input_pins + idx];
}
