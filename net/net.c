/**
* net.c
* 
* General networking implementation
*/

#include <stdio.h>
#include "msg.h"
#include "thread.h"
#include "xtimer.h"
#include "net/gnrc.h"
#include "net/gnrc/ipv6.h"
#include "net/gnrc/udp.h"
#include "cbor.h"

#include "../core.h"
#include "net.h"
#include "net_commands.h"
#include "net_submodule.h"

kernel_pid_t net_pid;
#define NET_QUEUE_SIZE  (8)
static char net_stack[THREAD_STACKSIZE_DEFAULT*2 + THREAD_EXTRA_STACKSIZE_PRINTF];

/* in expectation of a result */
static int core_flag;
static int core_pid;
static int core_operand1, core_operand2;
static Op_type core_operation;

/* status infos */
static int num_core_requests = 0;
static int num_core_requests_replied = 0;
static int num_network_requests = 0;
static int num_network_requests_replied = 0;


#ifndef NOCBOR
/* serialize data with cbor */
int serialize_payload( uint8_t *buf, int operation, int operand1, int operand2, int result) {
	
	memset(buf,0,MAX_PAYLOAD);
	cbor_stream_t stream;
	cbor_init(&stream, buf, MAX_PAYLOAD);
	cbor_serialize_int(&stream, operation);
	stream.pos = 7; // to be 4-Byte aligned
	cbor_serialize_int(&stream, operand1);
	stream.pos = 15;
	cbor_serialize_int(&stream, operand2);
	stream.pos = 23;
	cbor_serialize_int(&stream, result);
	int len = stream.pos;
	cbor_clear(&stream);
	cbor_destroy(&stream);
	return len;
}

/* deserialize data with cbor */
void deserialize_payload( uint8_t *buf, int *operation, int *operand1, int *operand2, int *result) {

	cbor_stream_t stream;
	cbor_init(&stream, buf, MAX_PAYLOAD);
	size_t offset = cbor_deserialize_int(&stream, 0, operation);
	offset += cbor_deserialize_int(&stream, 7, operand1);
	offset += cbor_deserialize_int(&stream, 15, operand2);
	offset += cbor_deserialize_int(&stream, 23, result);
	if( result != NULL)
	printf("   deserialized %d %d %d %d\n", *operation, *operand1, *operand2, *result);
	else
	printf("   deserialized %d %d %d\n", *operation, *operand1, *operand2);
    cbor_clear(&stream);
	cbor_destroy(&stream);
}
#endif

#ifndef NOSTATUS
/* register once for the kind of status data */
void register_status( void) {
	
	puts("core: register at border gateway");
	
    cbor_stream_t stream;
    static uint8_t cbor[256];
    cbor_init(&stream, cbor, 256);
    cbor_serialize_array(&stream, 2);
    cbor_serialize_unicode_string(&stream, "Calculator IoT");
    cbor_serialize_unicode_string(&stream, "");
    cbor_serialize_array(&stream, 2);
	cbor_serialize_unicode_string(&stream, "dev unicast addr");
	cbor_serialize_unicode_string(&stream, "string");
	cbor_serialize_array(&stream, 2);
	cbor_serialize_unicode_string(&stream, "is rpl root");
	cbor_serialize_unicode_string(&stream, "integer");
	cbor_serialize_array(&stream, 2);
	cbor_serialize_unicode_string(&stream, "can add");
	cbor_serialize_unicode_string(&stream, "integer");
	cbor_serialize_array(&stream, 2);
	cbor_serialize_unicode_string(&stream, "can sub");
	cbor_serialize_unicode_string(&stream, "integer");
	cbor_serialize_array(&stream, 2);
	cbor_serialize_unicode_string(&stream, "can mul");
	cbor_serialize_unicode_string(&stream, "integer");
	cbor_serialize_array(&stream, 2);
	cbor_serialize_unicode_string(&stream, "can div");
	cbor_serialize_unicode_string(&stream, "integer");
	cbor_serialize_array(&stream, 2);
	cbor_serialize_unicode_string(&stream, "core requests");
	cbor_serialize_unicode_string(&stream, "integer");
	cbor_serialize_array(&stream, 2);
	cbor_serialize_unicode_string(&stream, "core requests replied");
	cbor_serialize_unicode_string(&stream, "integer");
	cbor_serialize_array(&stream, 2);
	cbor_serialize_unicode_string(&stream, "network requests");
	cbor_serialize_unicode_string(&stream, "integer");
	cbor_serialize_array(&stream, 2);
	cbor_serialize_unicode_string(&stream, "network requests replied");
	cbor_serialize_unicode_string(&stream, "integer");

    int len = stream.pos;
    cbor_clear(&stream);
    cbor_destroy(&stream);
		
    netsub_sendto( BORDER_GATEWAY, (char*)cbor, len, COAP_METHOD_POST, "register");
}
#endif

#ifndef NOSTATUS
/* send cbor serialized status update */
void update_status( void) {
	
	puts("net: update status to border gateway");

    cbor_stream_t stream;
    static uint8_t cbor[256];
    cbor_init(&stream, cbor, 256);
    cbor_serialize_unicode_string(&stream, dev_unicast_addr);
    #ifdef ROOT 
    cbor_serialize_int(&stream, 1); 
    #else 
    cbor_serialize_int(&stream, 0);
    #endif
    #if (defined(CANADD) || defined(CANALL))
    cbor_serialize_int(&stream, 1); 
    #else 
    cbor_serialize_int(&stream, 0);
    #endif
    #if (defined(CANSUB) || defined(CANALL))
    cbor_serialize_int(&stream, 1); 
    #else 
    cbor_serialize_int(&stream, 0);
    #endif
    #if (defined(CANMUL) || defined(CANALL))
    cbor_serialize_int(&stream, 1); 
    #else 
    cbor_serialize_int(&stream, 0);
    #endif
    #if (defined(CANDIV) || defined(CANALL))
    cbor_serialize_int(&stream, 1); 
    #else 
    cbor_serialize_int(&stream, 0);
    #endif
	cbor_serialize_int(&stream, num_core_requests); 
	cbor_serialize_int(&stream, num_core_requests_replied); 
	cbor_serialize_int(&stream, num_network_requests); 
	cbor_serialize_int(&stream, num_network_requests_replied); 

    int len = stream.pos;
    cbor_clear(&stream);
    cbor_destroy(&stream);
	
	netsub_sendto( BORDER_GATEWAY, (char*)cbor, len, COAP_METHOD_PUT, "data");
}
#endif

/* process incoming udp packet */
void handle_udp_msg( msg_t *msg) {
    
    /* to be parsed */
   	Op_type msg_operation;
	int msg_operand1, msg_operand2;
	int result;

    gnrc_pktsnip_t *pktsnip = (gnrc_pktsnip_t*) msg->content.ptr;
        
    /* show ipv6 source address */
	ipv6_hdr_t *iphdr = (ipv6_hdr_t*) pktsnip->next->next->data;
	char ipv6_addr_src[IPV6_ADDR_MAX_STR_LEN];
	ipv6_addr_to_str(ipv6_addr_src, &iphdr->src, IPV6_ADDR_MAX_STR_LEN);
	printf("   Quelle des Pakets: %s\n", ipv6_addr_src);
    
    #ifndef NOCBOR
    uint8_t buf[MAX_PAYLOAD];
    #else
    char buf[MAX_PAYLOAD];
    #endif
    
	int payload_len;
    if( (payload_len = netsub_get_payload( (char*)buf, pktsnip)) == -1)
		return;
    
    /* show packet content */
    printf("   Inhalt des Pakets: ");
    #ifndef NOCBOR
    for(int i=0; i<payload_len; i++) printf("%02X ", (int) buf[i]);
    puts("");
    #else
    printf("%s, len %d\n", buf, strlen(buf));
    #endif
    
	/* packet is a calculation response */ 
	if ( netsub_is_response( pktsnip)) {
		
		puts("   calc Antwort");
		
		#ifdef NOCBOR
		sscanf(buf, "= %d %d %d %d", (int*)&msg_operation, &msg_operand1, &msg_operand2, &result);
		#else
		deserialize_payload( buf, (int*)&msg_operation, &msg_operand1, &msg_operand2, &result);
		#endif
		
		/* fits to what the core module is expecting */
		if( core_flag && msg_operand1==core_operand1 && msg_operation==core_operation && msg_operand2==core_operand2) {
			++num_core_requests_replied;
			
			msg_t reply;
			reply.type = CORE_MSG;
			reply.content.value = result;
			/* send to main thread */
			msg_send(&reply, core_pid);
		}
		
	/* packet is a calculation request */
    } else if( netsub_is_request( pktsnip)) {

		puts("   calc Anfrage");
		++num_network_requests;
		
		#ifdef NOCBOR
		sscanf(buf, "? %d %d %d", (int*)&msg_operation, &msg_operand1, &msg_operand2);
		#else
		deserialize_payload( buf, (int*)&msg_operation, &msg_operand1, &msg_operand2, NULL);
		#endif
		
		result = core_do_operation( msg_operation, msg_operand1, msg_operand2);

		/* could calculate result */ 
		if( result != -1) {
			++num_network_requests_replied;
			
			#ifdef NOCBOR
			char response[MAX_PAYLOAD];
			sprintf(response, "= %d %d %d %d", msg_operation, msg_operand1, msg_operand2, result);
			int len = strlen(response);
			#else
			uint8_t response[MAX_PAYLOAD];
			int len = serialize_payload( response, msg_operation, msg_operand1, msg_operand2, result);
			#endif
			
			/* send back result */
			netsub_reply( pktsnip, (char*)response, len);
		}
		
		/* boards without io update their status when having processed a packet */
        #ifndef NOSTATUS
		#if !(defined(IO_SW) || defined (IO_HW) || defined (IO_DUMMY))
		update_status();
		#endif
		#endif
    }
}

/* process a message from the main thread */
void handle_core_msg( msg_t *msg) {

    /* remember the last request */
    core_flag = 1;
    core_pid = msg->sender_pid;
   
    sscanf(msg->content.ptr, "? %d %d %d", (int*)&core_operation, &core_operand1, &core_operand2);
    
    #ifdef NOCBOR
    char* buf = (char*)msg->content.ptr;
    int len = strlen(msg->content.ptr);
    #else
    uint8_t buf[MAX_PAYLOAD];
    int len = serialize_payload( buf, core_operation, core_operand1, core_operand2, 0);
    #endif
    
    /* send request to other board */
    #ifdef NOCOAP
		#ifdef NORPL
			netsub_sendto( MULTICAST_ADDR(core_operation), buf, len );
		#else
			netsub_sendto( ANYCAST_ADDR(core_operation), buf, len );
		#endif
	#else
		#ifdef NORPL
			netsub_sendto( MULTICAST_ADDR(core_operation), (char*)buf, len, COAP_METHOD_GET, "calc");
		#else
			netsub_sendto( ANYCAST_ADDR(core_operation), (char*)buf, len, COAP_METHOD_GET, "calc");
		#endif
	#endif
}

/* net thread for processing messages */
void *net(void *arg)
{
    (void) arg;

    static msg_t _msg_q[NET_QUEUE_SIZE];	
    msg_t msg;

    msg_init_queue(_msg_q, NET_QUEUE_SIZE);
    /* register net thread for udp communication on port APP_PORT */
    gnrc_netreg_entry_t me_reg = { .demux_ctx = APP_PORT, .pid = thread_getpid() };
    gnrc_netreg_register(GNRC_NETTYPE_UDP, &me_reg);
        
    while (1) {
        puts("net: Anfang Message-Loop");
        puts("net: ifconfig");
        _netif_config(0, NULL);
        //~ puts("net: msg queue stats:");
        //~ msg_queue_print();
        //~ puts("net: gnrc stats:");
        //~ gnrc_pktbuf_stats();
        #ifndef NORPL
        puts("net: rpl fib table:");
        fib_print_routes(&gnrc_ipv6_fib_table);
        #endif
    
        memset(&msg, 0, sizeof(msg));
        msg_receive(&msg);
        switch (msg.type) {

            case CORE_MSG:

                printf("net: IPC Message von Core erhalten, %s, len %d\n",
					(char*)msg.content.ptr, strlen(msg.content.ptr));
                
                ++num_core_requests;

                handle_core_msg( &msg);

                break;		

            case GNRC_NETAPI_MSG_TYPE_RCV:

                puts("net: UDP Paket erhalten");
        
                handle_udp_msg( &msg);

			    gnrc_pktbuf_release( (gnrc_pktsnip_t*)msg.content.ptr);

                break;
    
            case GNRC_NETAPI_MSG_TYPE_SND:
                puts("net: UDP Paket gesendet");

                gnrc_pktbuf_release( (gnrc_pktsnip_t*)msg.content.ptr);
                
                break;
        
            case GNRC_NETAPI_MSG_TYPE_SET:
            case GNRC_NETAPI_MSG_TYPE_GET:
                puts("net: MSG_GET");
                msg_t reply;
                reply.type = GNRC_NETAPI_MSG_TYPE_ACK;
                reply.content.value = -ENOTSUP;
                msg_reply(&msg, &reply);
                break;
                
            default:
                puts("net: unexpected packet received");
                break;
        }
    }
    return NULL;
}

/* called from main thread at start */
int net_init( void) {

    net_pid = thread_create(net_stack, sizeof(net_stack), THREAD_PRIORITY_MAIN - 1, THREAD_CREATE_STACKTEST, net, NULL, "net");
    printf("core: net Thread created, pid %d\n", net_pid);

    // core msg-queue
    //~ static msg_t _msg_q[NET_QUEUE_SIZE];
    //~ msg_init_queue(_msg_q, NET_QUEUE_SIZE);
    
    /* initialize network utilities and get own ip address */
    cmd_init();

    /* without rpl: enlist operation specific local address */
    #ifdef NORPL

        #if (defined(CANADD) || defined(CANALL))
            cmd_add_multicast_addr( MULTICAST_ADDR(OP_ADD));
        #endif

        #if (defined(CANSUB) || defined(CANALL))
            cmd_add_multicast_addr( MULTICAST_ADDR(OP_SUB));
        #endif

        #if (defined(CANMUL) || defined(CANALL))
            cmd_add_multicast_addr( MULTICAST_ADDR(OP_MUL));
        #endif

        #if (defined(CANDIV) || defined(CANALL))
            cmd_add_multicast_addr( MULTICAST_ADDR(OP_DIV));
        #endif

    /* with rpl */
    #else

        /* init rpl as root node */
        #ifdef ROOT

            cmd_add_root_global_addr();
            cmd_init_rpl();
            cmd_rpl_root();
        
        /* init rpl as non-root node */
        #else
            cmd_init_rpl();
        #endif
        
        /* enlist operation specific address and route to localhost */
        #if (defined(CANADD) || defined(CANALL))
            cmd_add_anycast_addr( ANYCAST_ADDR(OP_ADD));
            cmd_add_route( ANYCAST_ADDR(OP_ADD), "::1");
        #endif

        #if (defined(CANSUB) || defined(CANALL))
            cmd_add_anycast_addr( ANYCAST_ADDR(OP_SUB));
            cmd_add_route( ANYCAST_ADDR(OP_SUB), "::1");
        #endif

        #if (defined(CANMUL) || defined(CANALL))
            cmd_add_anycast_addr( ANYCAST_ADDR(OP_MUL));
            cmd_add_route( ANYCAST_ADDR(OP_MUL), "::1");
        #endif

        #if (defined(CANDIV) || defined(CANALL))
            cmd_add_anycast_addr( ANYCAST_ADDR(OP_DIV));
            cmd_add_route( ANYCAST_ADDR(OP_DIV), "::1");
        #endif
        
        /* also enlist route with own ip address */
        cmd_add_route( dev_unicast_addr, "::1");
        
        fib_print_routes(&gnrc_ipv6_fib_table);

    #endif
    
    _netif_config(0, NULL);
      
    #ifndef NOSTATUS
	puts("Wait for RPL configuration...");
    xtimer_sleep(3);
	register_status();
    #endif

	return 0;
}

/* called from core module to do operation externally */
int net_ask_network( Op_type operation, int operand1, int operand2) {

    msg_t msg, reply;

    char tmp[MAX_PAYLOAD];
    sprintf(tmp, "? %d %d %d", operation, operand1, operand2);
    msg.content.ptr = tmp;
    msg.type = CORE_MSG;

    //~ msg_queue_print();

    /* send request to net thread without blocking */
    if ( !msg_try_send( &msg, net_pid))
        puts("core: Fehler beim Senden von IPC-Msg");
    
    int result;
    /* wait 3 seconds for response from net thread */
    if( xtimer_msg_receive_timeout( &reply, 3*1e6) < 0) {
        puts("core: keine Antwort vom Netzwerk nach Timeout");
        result = -1;
    } else {
        result = reply.content.value;
    }
    
    core_flag = 0;

    /* io boards update their status when receiving a result */
    #ifndef NOSTATUS
    #if (defined(IO_SW) || defined (IO_HW) || defined (IO_DUMMY))
    update_status();
    #endif
    #endif
        
    return result;
}
