/**
* net.h
* 
* General networking declarations
*/

#ifndef NET_H
#define NET_H

#include "net/ipv6/addr.h"
#include "../calc/calc.h" // for Op_type
 
/* called from core module at start, returns error code */
int net_init( void);

/* called from core module to do operation externally */
int net_ask_network( Op_type operation, int operand1, int operand2);

/* maximum message size */
#define MAX_PAYLOAD 	(32)

/* id for IPC messages */
#define CORE_MSG		(12345) 

#ifndef NOCOAP
	#define APP_PORT	(5683)
	//~ #define CLIENT_PORT	(9293)
	#define CLIENT_PORT	APP_PORT
#else
	#define APP_PORT	(13337)
	#define NOCBOR
	#define NOSTATUS
#endif

#ifdef NOCBOR
	#define NOSTATUS
#endif

#ifndef NOSTATUS 
	//~ #define BORDER_GATEWAY	("fe80::40d2:91ff:fe05:dcb9")
	#define BORDER_GATEWAY	("fe80::b")
#endif

/* own ip address */
extern char dev_unicast_addr[IPV6_ADDR_MAX_STR_LEN];

#endif
