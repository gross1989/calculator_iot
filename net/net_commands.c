/**
* net_commands.h
* 
* Networking utility implementation
*/

#include <stdio.h>

#include "net/gnrc.h"
#include "net/gnrc/ipv6.h"
#include "net/gnrc/udp.h"

#include "net_commands.h"

/* network interface number */
char dev_str[4];
/* own ip address */
char dev_unicast_addr[IPV6_ADDR_MAX_STR_LEN];

#ifndef NORPL

/* enlist rpl root address */
void cmd_add_root_global_addr( void) {

	char* params[] = {NULL, dev_str, "add", RPL_ROOT_GLOBAL_ADDR};
	_netif_config(4, params);
}

/* initialize rpl */
void cmd_init_rpl( void) {
	
	char* params[] = {NULL, "init", dev_str};
	_gnrc_rpl(3, params);
}

/* set rpl root address */
void cmd_rpl_root( void) {
	
	char *params[] = {NULL, "root", "1", RPL_ROOT_GLOBAL_ADDR};
	_gnrc_rpl(4, params);	
}

#endif

/* enlist anycast address */
void cmd_add_anycast_addr( char* addr) {
	
	char* params[] = {NULL, dev_str, "add", "anycast", addr};
	_netif_config(5, params);
}

/* enlist multicast address */
void cmd_add_multicast_addr( char* addr) {

	char* params[] = {NULL, dev_str, "add", "multicast", addr};
	_netif_config(5, params);
}


#ifndef NORPL
/* add a route to the fib */
void cmd_add_route( char *dst, char *next) {

	char* params[] = {NULL, "add", dst, "via", next};
	_fib_route_handler(5, params);
}

#endif

/* get own local unicast address */
ipv6_addr_t * cmd_get_unicast_addr( void) {
	
	gnrc_ipv6_netif_t *entry = gnrc_ipv6_netif_get( atoi(dev_str));

	for (int i = 0; i < GNRC_IPV6_NETIF_ADDR_NUMOF; i++) {
		if (!ipv6_addr_is_unspecified(&entry->addrs[i].addr)) {

			//~ #ifdef NORPL
			if ((ipv6_addr_is_link_local(&entry->addrs[i].addr))
				&& !(entry->addrs[i].flags & GNRC_IPV6_NETIF_ADDR_FLAGS_NON_UNICAST)) {
			//~ #else
			//~ if ((ipv6_addr_is_global(&entry->addrs[i].addr))
				//~ && !(entry->addrs[i].flags & GNRC_IPV6_NETIF_ADDR_FLAGS_NON_UNICAST)) {
			//~ #endif
				return &entry->addrs[i].addr;
			}
		}
	}
	return NULL;
}

/* initialize network utilities and get own ip address */
void cmd_init( void) {

	kernel_pid_t ifs[GNRC_NETIF_NUMOF];
	gnrc_netif_get(ifs);
	sprintf(dev_str, "%d", ifs[0]);
	
    /* own ip address without rpl: fe80::e822:7eff:fee4:4223/64  scope: local */
	ipv6_addr_t *ipaddr = cmd_get_unicast_addr();
	ipv6_addr_to_str(dev_unicast_addr, ipaddr, IPV6_ADDR_MAX_STR_LEN);
	
	#ifndef NORPL
		#ifdef ROOT
        /* own ip address for rpl root: 2001:db8::1 */
        strncpy(dev_unicast_addr, RPL_ROOT_GLOBAL_ADDR, IPV6_ADDR_MAX_STR_LEN);
		#else
        /* own ip address for rpl non-root: 2001:db8::e822:7eff:fee4:4223/64  scope: global */
		char tmp[IPV6_ADDR_MAX_STR_LEN] = {0};
		strncpy(tmp, PREFIX_GLOBAL_UNICAST, strlen(PREFIX_GLOBAL_UNICAST));
		strcat(tmp, dev_unicast_addr+strlen(PREFIX_LOCAL_UNICAST));
		strncpy(dev_unicast_addr, tmp, IPV6_ADDR_MAX_STR_LEN);
		#endif
	#endif
	
	printf("dev unicast addr: %s\n", dev_unicast_addr);
}

//~ void cmd_del_addr( char* addr) {
//~ 
	//~ char* params[] = {NULL, "6", "del", addr};
	//~ _netif_config(4, params);
//~ }
