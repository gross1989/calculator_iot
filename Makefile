# name of your application
APPLICATION = calculator_iot

# If no BOARD is found in the environment, use this default:
BOARD ?= native

# This has to be the absolute path to the RIOT base directory:
RIOTBASE ?= $(CURDIR)/../RIOT

# Uncomment this to enable scheduler statistics for ps:
#CFLAGS += -DSCHEDSTATISTICS

# If you want to use native with valgrind, you should recompile native
# with the target all-valgrind instead of all:
# make -B clean all-valgrind

# Comment this out to disable code in RIOT that does safety checking
# which is not needed in a production environment but helps in the
# development process:
CFLAGS += -DDEVELHELP 
#~ -DTHREAD_STACKSIZE_MAIN=2048

# Change this to 0 show compiler invocation lines by default:
QUIET ?= 1

# Modules to include:
USEMODULE += shell
USEMODULE += xtimer
USEMODULE += shell_commands
USEMODULE += ps
USEMODULE += auto_init
USEMODULE += config

# include and auto-initialize all available sensors
USEMODULE += saul_reg
USEMODULE += saul_default
USEMODULE += auto_init_saul

DIRS += calc
USEMODULE += calc

DIRS += net
USEMODULE += net

# module for input/output
ifneq (,$(filter -DIO_DUMMY,$(CFLAGS)))
	DIRS += io/iodummy
	USEMODULE += iodummy
endif
ifneq (,$(filter -DIO_SW,$(CFLAGS)))
	DIRS += io/swio
	USEMODULE += swio
endif
ifneq (,$(filter -DIO_HW,$(CFLAGS)))
	DIRS += io/hwio
	USEMODULE += hwio
endif

BOARD_PROVIDES_NETIF := airfy-beacon fox iotlab-m3 mulle native nrf51dongle \
	nrf6310 pba-d-01-kw2x pca10000 pca10005 saml21-xpro samr21-xpro spark-core \
	yunjia-nrf51822

# networking available
ifneq (,$(filter $(BOARD),$(BOARD_PROVIDES_NETIF)))

	# sufficient ip addresses
	CFLAGS += -DGNRC_IPV6_NETIF_ADDR_NUMOF=12

	# gnrc is a meta module including all required, basic gnrc networking modules
	USEMODULE += gnrc
	# use the default network interface for the board
	USEMODULE += gnrc_netdev_default
	# automatically initialize the network interface
	USEMODULE += auto_init_gnrc_netif
	# the application dumps received packets to stdout
	USEMODULE += gnrc_pktdump

	USEMODULE += gnrc_netdev_default
	USEMODULE += auto_init_gnrc_netif
	USEMODULE += gnrc_icmpv6_echo
	#~ USEMODULE += posix_sockets

	# RPL enabled
	ifeq (,$(filter -DNORPL,$(CFLAGS)))
		USEMODULE += gnrc_rpl
		USEMODULE += gnrc_ipv6_router_default
		USEMODULE += fib
	else
	# RPL disabled
		USEMODULE += gnrc_ipv6_default
		# We use only the lower layers of the GNRC network stack, hence, we can
		# reduce the size of the packet buffer a bit
		#~ CFLAGS += -DGNRC_PKTBUF_SIZE=512
	endif

	# COAP enabled
	ifeq (,$(filter -DNOCOAP,$(CFLAGS)))
		USEMODULE += gnrc_conn_udp
		USEPKG += microcoap
		#~ CFLAGS += -DMICROCOAP_DEBUG
		DIRS += net/coapnet
		USEMODULE += coapnet

		# CBOR enabled
		ifeq (,$(filter -DNOCBOR,$(CFLAGS)))
			USEMODULE += cbor
			#CFLAGS += -DCBOR_NO_PRINT
		endif
	
	else
	# COAP disabled
		USEMODULE += gnrc_udp
		DIRS += net/msgnet
		USEMODULE += msgnet
	endif

endif


FEATURES_OPTIONAL += config
FEATURES_OPTIONAL += periph_rtc

# recompile modules that can be influenced by cflags
# NORPL
$(shell rm -fr bin/native/gnrc_ipv6*)
$(shell rm -fr bin/$(BOARD)/gnrc_ipv6*)
$(shell rm -fr bin/native/gnrc_ndp*)
$(shell rm -fr bin/$(BOARD)/gnrc_ndp*)
$(shell rm -fr bin/native/gnrc_sixlowpan*)
$(shell rm -fr bin/$(BOARD)/gnrc_sixlowpan*)
$(shell rm -fr bin/native/gnrc_icmpv6*)
$(shell rm -fr bin/$(BOARD)/gnrc_icmpv6*)
$(shell rm -fr bin/native/shell_commands)
$(shell rm -fr bin/$(BOARD)/shell_commands)

#~ IO_SW, IO_HW, CAN, NOCOAP, NORPL
$(shell rm -fr bin/native/calculator_iot)
$(shell rm -fr bin/$(BOARD)/calculator_iot)
$(shell rm -fr bin/native/calc) 
$(shell rm -fr bin/$(BOARD)/calc)
$(shell rm -fr bin/native/msgnet)
$(shell rm -fr bin/$(BOARD)/msgnet)
$(shell rm -fr bin/native/net)
$(shell rm -fr bin/$(BOARD)/net)
$(shell rm -fr bin/native/coapnet)
$(shell rm -fr bin/$(BOARD)/coapnet)

#~ OUT_SEG 
$(shell rm -fr bin/native/hwio)
$(shell rm -fr bin/$(BOARD)/hwio)

include $(RIOTBASE)/Makefile.include

