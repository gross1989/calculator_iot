

1. Einleitung

    1. Team
    2. Projekt
    3. Anwendungsfälle

2. Entwurf
    1. Modularisierung
    2. Hauptprogramm
    3. einzelne Module
    
3. Implementierung (lessons learnt über RIOT, aufgetretene Probleme, Kompillierzeitabhängigkeit über CFLAGS)
    
    1. Buildsystem
    2. calc
    3. net
        1. RPL
        2. COAP
        3. CBOR
        4. Border Gateway
    4. swio
    5. hwio
  
4. Zusammenfassung (Fazit, Herausforderungen, Aussichten)